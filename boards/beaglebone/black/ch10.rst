.. _beagleboneblack-pictures:

Pictures
#############


.. figure:: images/DSC00481.png
   :width: 768px
   :height: 476px
   :align: center
   :alt: Top Side

   Top Side

.. figure:: images/DSC00484.png
   :width: 768px
   :height: 476px
   :align: center
   :alt: Bottom Side

   Bottom Side

.. figure:: images/DSC00505.png
   :width: 768px
   :height: 768px
   :align: center
   :alt: 45 Degree Top

   45 Degree Top

