.. _beagleconnect-freedom-quick-start:

Quick Start Guide
####################

What's included in the box?
****************************

1. BeagleConnect Freedom board in enclosure
2. Antenna
3. USB cable
4. Quick-start card

.. todo::

   Image with what's inside the box and a better description.


Attaching antenna
******************

.. todo::

   Describe how to screw on the antenna.


Tethering to PC
****************

.. todo::

   Describe how to get a serial connection.

Wireless Connection
********************

.. todo::

   Describe how to get an IEEE802.15.4g connection from BeaglePlay.


Access Micropython
*******************

Boards come pre-flashed with Micropython. Read :ref:`beagleconnect-freedom-using-micropython` for
more details.

.. todo::

   Describe how to get to a local console and websockets console.


Demos and Tutorials
*******************

* :ref:`beagleconnect-freedom-using-greybus`
* :ref:`beagleconnect-freedom-using-micropython`
* :ref:`beagleconnect-freedom-using-zephyr`
