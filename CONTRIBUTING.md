# Contributing to docs.beagleboard.io

**First off, thanks for taking the time to think about contributing!**

The following is a set of guidelines for contributing to docs.beagleboard.io, which is hosted by
the BeagleBoard.org Foundation at https://git.beagleboard.org/docs/docs.beagleboard.io. These are
mostly guidelines, not rules. Use your best judgment, and feel free to propose changes to this
document in a pull request.

## Code of Conduct

This project and everyone participating in it is governed by
the [BeagleBoard.org Code of Conduct](CODE_OF_CONDUCT.md). By participating, you are expected to
uphold this code. Please report unacceptable behavior to [coc@bbb.io](mailto:coc@bbb.io) or
contact one of the administrators on https://forum.beagleboard.org.

## FAQ

* [Frequently Asked Questions category on the BeagleBoard.org Forum](https://forum.beagleboard.org/c/faq/19)

See more at https://docs.beagleboard.org/en/latest/intro/contribution/.
